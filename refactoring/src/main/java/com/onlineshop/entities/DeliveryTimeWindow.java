package com.onlineshop.entities;

import java.time.LocalDate;
import java.time.LocalTime;

public class DeliveryTimeWindow {
    private LocalDate start;
    private LocalDate end;

    public DeliveryTimeWindow(LocalDate start, LocalDate end){
        this.start = start;
        this.end = end;
    }

    public static DeliveryTimeWindow deliveryTimeWindow(){
        return new DeliveryTimeWindow();
    }

    public DeliveryTimeWindow startInDays(int days){
        this.start = getStart().plusDays(days);
        return this;
    }

    public DeliveryTimeWindow endInDays(int days){
        this.end = getEnd().plusDays(days);
        return this;
    }

    public LocalDate getStart() {
        return start;
    }

    public void setStart(LocalDate start) {
        this.start = start;
    }

    public LocalDate getEnd() {
        return end;
    }

    public void setEnd(LocalDate end) {
        this.end = end;
    }

    private DeliveryTimeWindow(){
        LocalDate now = LocalDate.now();
        this.start = now;
        this.end = now;
    }
}
